#ifndef __OldTProc_H__
#define __OldTProc_H__
#include <iostream>
#include <locale>
#include "C:\mp2-lab4-queue\src\TQueue\TQueue.h"
#include "C:\mp2-lab4-queue\src\TJobStream\TJobStream.h"

using namespace std;

class OldTProc
{
private:
	TJobStream JobStream;
	TQueue Queue;
	bool unlocked_first_proc;
	int takts;
	int TaskJobs, TaskRefusing,TaskDownTime_1;
	int FirstProc, SecondProc;
public:
	OldTProc(int _q1, int _q2, int _size,
		int _takts) :JobStream(_q1, _q2), Queue(_size),
		takts(_takts)
	{
		setlocale(LC_ALL, "Russian");
		unlocked_first_proc = true;
		TaskJobs = 0;
		TaskRefusing = 0;
		TaskDownTime_1 = 0;
	}
	void Service()
	{
		int Task;
		if (Task = JobStream.CreateJob())
		{
			Queue.Put(Task);
			//���� ������� �����, �� ������� ������ 
			if (Queue.GetRetCode() == DataFull)
			{
				TaskRefusing++;
			}
		}
		if (unlocked_first_proc)
		{
			FirstProc = Queue.Get();
			//���� ������� �����, �� ������ ��������� �����������; 
			if (Queue.GetRetCode() == DataEmpty)
				TaskDownTime_1++;
			else
			{
				TaskJobs++;
				unlocked_first_proc = false;
			}
		}
		if (!unlocked_first_proc)
			unlocked_first_proc = JobStream.EndJob();
	}

	void PrintInfo()
	{
		for (int i = 0; i<takts; i++)
		{
			Service();
		}
		cout << endl;
		cout << "���������� ����������� � �� ������� � ������� ����� �������� ��������: " << JobStream.GetID() << endl;
		cout << "���������� ������� ������������ �����������: " << TaskJobs << endl;
		cout << "���������� ������� �����������, �� �� ������������ �����������: " << JobStream.GetID() - TaskJobs << endl;
		cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: " << TaskRefusing << endl;
		cout << "���������� ������ ������� ����������: " << TaskDownTime_1 << endl;
		cout << "������� ������� ������� ����������: " << ((takts) ? floor(TaskDownTime_1 * 100 / takts) : 0) << "%" << endl;
	}
};
#endif

